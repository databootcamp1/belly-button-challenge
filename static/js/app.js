// Use the D3 library to read samples.json from the URL provided in the challenge
const url = "https://2u-data-curriculum-team.s3.amazonaws.com/dataviz-classroom/v1.1/14-Interactive-Web-Visualizations/02-Homework/samples.json";

// Variable to store full data
let fullData = {};

// Fetch the JSON data and console log it
d3.json(url).then(function(data) {
    console.log(data);
    fullData = data;

    // Sample data (replace this with your actual data)
    const dropdownValues = data.samples.map(item => item.id);

    // Select the dropdown element by id
    const dropdown = d3.select("#selDataset");

    // Bind the data to the dropdown
    const options = dropdown
    .selectAll("option")
    .data(dropdownValues)
    .enter()
    .append("option");

    // Set the text and values for the options
    options.text(d => d).attr("value", d => d);

    // Select the first value
    optionChanged(data.samples[0].id);
});

// Function for plotting a bar chart
function plotBar(changeValue){
    console.log('optionChanged', changeValue);
    const samples = fullData.samples;

    const selectedValues = samples.filter(sample =>{return sample.id === changeValue})[0]

    // Create a horizontal bar chart with a dropdown menu to display the top 10 OTUs found in that individual.
    // Trace1 for the Data
    console.log('selectedValues', selectedValues);
    // Create an array of objects with id, value, and label properties and also sort them
    let dataPoints = selectedValues.otu_ids.slice(0, 10).map((id, index) => ({
        id,
        value: selectedValues.sample_values[index],
        label: selectedValues.otu_labels[index]
    })).sort((a, b) => a.value - b.value);


  let trace1 = {
    x: dataPoints.map(item => item.value),
    y: dataPoints.map(item => "OTU: " + item.id),
    text: dataPoints.map(item => item.label), 
    type: "bar",
    orientation: "h"
};

      
      // Data array
      let tableData = [trace1];
      
      // Apply a title to the layout
      let layout = {
        margin: {
          l: 100,
          r: 100,
          t: 100,
          b: 100
        }
      };
      
      // Render the plot to the div tag with id "bar"
      Plotly.newPlot("bar", tableData, layout);
}

//Function for plotting bubble chart
function plotBubble(changeValue){
    console.log('optionChanged', changeValue);
    const samples = fullData.samples;
    const selectedValues = samples.filter(sample =>{return sample.id === changeValue})[0];
    
    
    // Create an array of objects with id, value, and label properties and also sort them
    let dataPoints = selectedValues.otu_ids.map((id, index) => ({
        id,
        value: selectedValues.sample_values[index],
        label: selectedValues.otu_labels[index]
    })).sort((a, b) => a.value - b.value);


    // Define a color scale 
    const colorScale = d3.scaleOrdinal(d3.schemeCategory10);

    let trace1 = {
        x: dataPoints.map(item => item.id),
        y: dataPoints.map(item => item.value),
        text: dataPoints.map(item => item.label), 
        mode: 'markers',
        marker: {
            size: dataPoints.map(item => item.value),
            sizemode: 'diameter',
            sizeref: 2.5,
            color: dataPoints.map(item => colorScale(item.id))
        }
      };
      
      let data = [trace1];
      
      let layout = {
        showlegend: false, 
      };
      
      Plotly.newPlot('bubble', data, layout);

}

// Function for populating metadata
function populateMetadata(changeValue){
    console.log('optionChanged', changeValue);
    const metadata = fullData.metadata;
    const selectedValues = metadata.filter(demoInfo =>{return demoInfo.id == changeValue})[0];

    let metadataSelection = d3.select("#sample-metadata");

    // Clear existing content
    metadataSelection.html("");
    
    // Loop through the metadata and append new elements
    Object.entries(selectedValues).forEach(([field, value]) => {
        metadataSelection
          .append("p")  
          .text(`${field}: ${value}`);
      });
}

// Change dropdown list value
function optionChanged(changeValue) {
    plotBar(changeValue);
    plotBubble(changeValue);
    populateMetadata(changeValue);
}



